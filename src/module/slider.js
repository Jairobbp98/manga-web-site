const d = document;

const Slider = () => {
    const $elementosLista = d.querySelector('.carousel-slider'),
          opciones = d.querySelector('.carrousel-options');
    
    new Glider($elementosLista, {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: opciones,
        scrollLock: true,
        draggable: true
    });    
}

export { Slider }